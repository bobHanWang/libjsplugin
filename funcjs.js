let { log } = console;

const keyA = {a: 1};
const keyB = {b: 2};
const keyC = {b: 2};

const myObject = {
  [keyA]: 'valueA',
  [keyB]: 'valueB',
  [keyC]: 'valueC'
};

log(myObject)